/*Переменная хранит в себе значение от 0 до 9. 
Написать скрипт который будет выводить слово “один”, 
если переменная хранит значение 1. Выводить слово “два” - 
если переменная хранит значение 2, и т.д. для всех цифр от 0 до 9. 
Реализовать двумя способами.*/

// Способ 1
var a = 5;
if (a==0){
    console.log('ноль');
} else if(a==1){
    console.log('один'); 
} else if (a==2){
    console.log('два');
} else if (a==3) {
    console.log('три');
} else if (a==4) {
    console.log('четыре');
} else if (a==5) {
    console.log('пять');
} else if (a==6) {
    console.log('шесть');
} else if (a==7) {
    console.log('семь');
} else if (a==8) {
    console.log('восемь');
} else if (a==9) {
    console.log('девять');
} else{
    console.log('переменная принимает значение вне диапазона цифр от 0 до 9');
};


// Способ 2

switch(a){
    case 0: res='ноль'; break;
    case 1: res='один'; break;
    case 2: res='два'; break;
    case 3: res='три'; break;
    case 4: res='четыре'; break;
    case 5: res='пять'; break;
    case 6: res='шесть'; break;
    case 7: res='семь'; break;
    case 8: res='восемь'; break;
    case 9: res='девять'; break;
    default: res = 'переменная принимает значение вне диапазона цифр от 0 до 9'; 
}
console.log(res);


//Способ 3

m=["ноль","один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"];
if ( (a>=0) && (a<10) && ((a-Math.trunc(a))==0)) {
console.log(m[a]);
} else {
    console.log("мне не нравится это значение переменной!");
};


